def sliding_down(right, down):
    text_file = open(".\input.txt", "r")
    list1 = text_file.readlines()
    location = 0
    count = 0
    for i in range(len(list1)-down):
        x = i + down
        if x % down == 0:
            location += right
            row = list1[x].strip("\n") * 100000
            if row[location] == "#":
                count += 1
    return count

slope1 = sliding_down(1, 1)
slope2 = sliding_down(3, 1)
slope3 = sliding_down(5, 1)
slope4 = sliding_down(7, 1)
slope5 = sliding_down(1, 2)
print(slope2)

print(slope1*slope2*slope3*slope4*slope5)
