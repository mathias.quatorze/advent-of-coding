with open('input.txt') as f:
    contents = f.read()

passports = contents.split('\n\n')
required_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
valid_count = 0
for passport in passports:
    res = all(field in passport for field in required_fields)
    if res == True: 
        valid_count += 1

print(valid_count)
