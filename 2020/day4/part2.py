import re

with open('input.txt') as f:
    contents = f.read()

passports = contents.split('\n\n')
required_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
valid_count = 0

def validate_birth_year(input):
    year = input.split(":")[1]
    if year.isdigit():
        if 1920 <= int(year) <= 2002:
            return True
        else:
            return False
    else:
        return False

def validate_issue_year(input):
    year = input.split(":")[1]
    if year.isdigit():
        if 2010 <= int(year) <= 2020:
            return True
        else:
            return False
    else:
        return False

def validate_expiration_year(input):
    year = input.split(":")[1]
    if year.isdigit():
        if 2020 <= int(year) <= 2030:
            return True
        else:
            return False
    else:
        return False

def validate_height(input):
    height = input.split(":")[1]
    if "cm" in height and len(height) == 5:
        if 150 <= int(height[0:3]) <= 193:
            return True
        else:
            return False
    elif "in" in height and len(height) == 4:
        if 59 <= int(height[0:2]) <= 76:
            return True
        else:
            return False
    else:
        return False

def validate_hair_color(input):
    color = input.split(":")[1]
    if "#" in color[0] and len(color) == 7:
        if re.match("^[a-f0-9_-]*$", color[1:8]):
            return True
        else:
            return False
    else:
        return False

def validate_eye_color(input):
    valid_colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
    color = input.split(":")[1]
    if any(field in color for field in valid_colors):
        return True
    else:
        return False

def validate_passport_id(input):
    pid = input.split(":")[1]
    if pid.isdigit() and len(pid) == 9:
        return True
    else:
        return False

for passport in passports:
    res = all(field in passport for field in required_fields)
    if res == True:
        fields = passport.split()

        # Birth Year Validation
        byr = [string for string in fields if "byr" in string]
        valid_byr = validate_birth_year(byr[0])

        # Issue Year Validation
        iyr = [string for string in fields if "iyr" in string]
        valid_iyr = validate_issue_year(iyr[0])

        # Expiration Year Validation
        eyr = [string for string in fields if "eyr" in string]
        valid_eyr = validate_expiration_year(eyr[0])

        # Height Validation
        hgt = [string for string in fields if "hgt" in string]
        valid_hgt = validate_height(hgt[0])

        # Hair Color Validation
        hcl = [string for string in fields if "hcl" in string]
        valid_hcl = validate_hair_color(hcl[0])

        # Eye Color Validation
        ecl = [string for string in fields if "ecl" in string]
        valid_ecl = validate_eye_color(ecl[0])

        # Passport Number Validation
        pid = [string for string in fields if "pid" in string]
        valid_pid = validate_passport_id(pid[0])

        # Check Validations
        if valid_byr == True and valid_iyr == True and valid_eyr == True and valid_hgt == True and valid_hcl == True and valid_ecl == True and valid_pid == True:
            valid_count += 1

print(valid_count)