text_file = open(".\input.txt", "r")
list_of_ids = text_file.readlines()

seat_id_list = []

for i in range(len(list_of_ids)):
    pass_id = list_of_ids[i].strip()
    rows = list(range(128))
    columns = list(range(8))
    for letter in pass_id[:7]:
        if letter == "F":
            rows = rows[:(int(len(rows)/2))]
        elif letter == "B":
            rows = rows[(int(len(rows)/2)):]
    for letter in pass_id[7:]:
        if letter == "L":
            columns = columns[:(int(len(columns)/2))]
        elif letter == "R":
            columns = columns[(int(len(columns)/2)):]
    seat_id = (rows[0] * 8) + columns[0]

    seat_id_list.append(seat_id)

print(max(seat_id_list))
