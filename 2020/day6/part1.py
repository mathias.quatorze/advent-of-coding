with open('input.txt') as f:
    contents = f.read()

group_answers = contents.split('\n\n')
count = 0

for answer in group_answers:
    string = answer.replace("\n", "")
    s=set(string)  #Creates a set of Unique Un-Ordered Elements
    l=len(s)       #It returns the length of the above set.
    count += int(l)

print(count)