with open('input.txt') as f:
    contents = f.read()

group_answers = contents.split('\n\n')
count = 0

for answer in group_answers:
    group_count = 0
    ans_list = answer.split("\n")
    group_size = int(len(ans_list))
    for letter in [chr(x) for x in range(ord('a'), ord('z')+1)]:
        res = list(filter(lambda x: letter in x, ans_list))
        if int(len(res)) == group_size:
            group_count += 1
    count += group_count

print(count)