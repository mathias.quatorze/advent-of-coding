from collections import defaultdict

text_file = open(".\input.txt", "r")
rules = text_file.readlines()

nested = defaultdict(set)
for r in rules:
    rules_split = r.strip().split(" bags contain ")
    contents = rules_split[1].split(",")
    outer_bag = rules_split[0]
    for c in contents:
        content = c.strip().replace(".", "").split(" ")
        content_color = content[1] + " " + content[2]
        if content != "no other bags":
            nested[content_color].add(outer_bag)

def potential_containers(bag_color):
    contained_in = nested[bag_color]
    if len(contained_in) == 0:
        return contained_in
    return contained_in.union(c2 for c in contained_in for c2 in potential_containers(c))

print(len(potential_containers('shiny gold')))

