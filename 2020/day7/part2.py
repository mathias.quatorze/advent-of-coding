from collections import defaultdict, deque

colors = defaultdict(list)
text_file = open(".\input.txt", "r")
rules = text_file.readlines()\

def total_count(root):
    total = 0
    for num, color in colors[root]:
        total += num + (num * total_count(color))
    return total

for r in rules:
    container, contained = [s for s in r.split(' bags contain ')]

    for bag in contained.replace('.', '').replace("\n", "").split(', '):
        if bag != 'no other bags':
            num = int(bag[0])
            child = bag[2:].split(' bag')[0].strip()
            colors[container].append((num, child)) # Contains child colors for each color
            
print('Part 2:', total_count('shiny gold'))
