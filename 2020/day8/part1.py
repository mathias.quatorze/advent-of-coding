text_file = open(".\input.txt", "r")
commands = text_file.readlines()

execution_order = []
accumulator = 0

i = 0

def define_order(pos):
    execution_order.append(pos)

while i not in execution_order:
    c = commands[i].strip()
    operation, count = c.split(" ")
    define_order(i)
    if operation == "jmp":
        i += int(count)
    else:
        i += 1

for j in execution_order:
    c = commands[j].strip()
    operation, count = c.split(" ")
    print(c)
    if operation == "acc":
        accumulator += int(count)

print(execution_order)
print(accumulator)