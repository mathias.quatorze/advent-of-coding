text_file = open(".\input.txt", "r")
total_list = text_file.readlines()
preamble = 25

def isSum(list_of_preamble, number):
    number = int(number.strip())
    for x in list_of_preamble:
        x = int(x.strip())
        for y in list_of_preamble:
            y = int(y.strip())
            if x+y==number:
                return True
    return False

for i in range(len(total_list)):
    list_of_preamble = []
    if i < (preamble):
        print("First " + str(preamble) + " digits, skipping...")
    else:
        number = total_list[i]
        list_of_preamble = total_list[i-preamble:i]
        valid = isSum(list_of_preamble, number)
        if valid is False:
            print(number.strip())