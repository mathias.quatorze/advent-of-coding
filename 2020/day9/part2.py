text_file = open(".\input.txt", "r")
total_list = [int(n) for n in text_file]
preamble = 25

n = 2
def isSum(list_of_preamble, number):
    number = int(number)
    for x in list_of_preamble:
        x = int(x)
        for y in list_of_preamble:
            y = int(y)
            if x+y==number:
                return True
    return False

def findSubArray(n):
    for i in range(len(total_list)-(n+1)):
        contiguous = total_list[i:(i+n)]
        if sum(contiguous) == invalid_number:
            return contiguous
    return False


for i in range(len(total_list)):
    dummy = 0
    list_of_preamble = []
    if i < (preamble):
        dummy += 1
    else:
        number = total_list[i]
        list_of_preamble = total_list[i-preamble:i]
        valid = isSum(list_of_preamble, number)
        if valid is False:
            invalid_number = number

print("Part 1 solution: " + str(invalid_number))

while n>1:
    if findSubArray(n):
        contiguous_set = findSubArray(n)
        break
    n += 1

weakness = (min(contiguous_set) + max(contiguous_set))
print(weakness)